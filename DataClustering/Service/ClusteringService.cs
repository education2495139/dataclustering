﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataClustering.Algorithms.KMeans;
using DataClustering.Algorithms;
using System.Diagnostics;
using ReactiveUI;

namespace DataClustering.Models;

public sealed class ClusteringService
{
    private static volatile ClusteringService instance;
    private static object syncRoot = new object();

    private ClusteringService() { }

    public static ClusteringService Instance
    {
        get
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new ClusteringService();
                }
            }

            return instance;
        }
    }

    private IDataClusterizer<DataVector> kMeansClusterizer;
    public IDataClusterizer<DataVector> KMeansClusterizer
    {
        get => kMeansClusterizer;
        private set => kMeansClusterizer = value;
    }

    public Action UpdateData;

    public void CreateKMeansClusterizer(DataVector[] data, int countClusters = 2)
    {
        KMeansClusterizer = new KMeansClusterizer(data, countClusters);
        UpdateData?.Invoke();
    }
}
