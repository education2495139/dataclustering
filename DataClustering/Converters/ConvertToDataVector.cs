﻿using Avalonia.Controls;
using DataClustering.Algorithms.KMeans;
using System;
using System.Linq;

namespace DataClustering.Converters;

public static class ConvertToDataVector
{
    public static CanvasData FromCSV(string rawData)
    {
        var splittedString = rawData
            .Split(Environment.NewLine);

        var headString = splittedString
            .First()
            .Split(";");

        var isHasHead = !double.TryParse(headString[0], out _);

        var data = splittedString
            .Skip(isHasHead ? 0 : 1)
            .Where(line => line.Length > 0)
            .Select(item => new DataVector(item.Split(";").Select(num => double.Parse(num)).ToArray()))
            .ToArray();

        return new CanvasData
        {
            NameAxisX = isHasHead ? headString[0].ToString() : string.Empty,
            NameAxisY = isHasHead ? headString[1].ToString() : string.Empty,
            Vector = data,
        };
    }

    public static DataVector[] FromXLS(string rawData)
    {
        return rawData
            .Split("|")
            .Where(line => line.Length > 0)
            .Select(item => new DataVector(item.Split(";").Select(num => double.Parse(num)).ToArray()))
            .ToArray();
    }
}
