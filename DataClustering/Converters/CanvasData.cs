﻿using DataClustering.Algorithms;
using DataClustering.Algorithms.KMeans;
using System;
using System.Linq;

namespace DataClustering.Converters;

public struct CanvasData
{
    public string NameAxisX;
    public string NameAxisY;
    public DataVector[] Vector;
}
