﻿using Avalonia;
using Avalonia.Media;
using DataClustering.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;

namespace DataClustering.ViewModels;

public class GridLineViewModel : ViewModelBase
{
    public GridLineViewModel(Point start, Point end, string axisLabel)
    {
        Start = start;
        End = end;
        AxisLabel = axisLabel;
    }

    public GridLineViewModel(Point start, Point end, string axisLabel, IBrush? brush) : this(start, end, axisLabel)
    {
        Brush = brush;
    }

    public Point Start { get; }

    public Point End { get; }

    public string AxisLabel { get; }

    public IBrush? Brush { get; } = Brushes.LightGray;

}
