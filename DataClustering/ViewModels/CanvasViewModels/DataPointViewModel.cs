﻿using Avalonia;
using Avalonia.Media;
using DataClustering.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;

namespace DataClustering.ViewModels;

public class DataPointViewModel : ViewModelBase
{
    public DataPointViewModel(double x, double y)
    {
        X = x;
        Y = y;
    }

    public DataPointViewModel(double x, double y, IBrush brush) : this(x, y)
    {
        Brush = brush;
    }

    public double X { get; }

    public double Y { get; }

    public IBrush? Brush { get; } = Brushes.Black;
}
