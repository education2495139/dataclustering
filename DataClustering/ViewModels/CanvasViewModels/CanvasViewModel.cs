﻿using Avalonia;
using Avalonia.Media;
using DataClustering.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;

namespace DataClustering.ViewModels;

public class CanvasViewModel : ViewModelBase
{
    private const int deadZone = 20;
    private const int countGridLines = 10;
    private double canvasWidth = 640;
    private double canvasHeight = 640;
    private ObservableCollection<GridLineViewModel> gridLines = new();
    private ObservableCollection<DataPointViewModel> manualCentroids = new();
    private ObservableCollection<DataPointViewModel> dataPointList = new();
    private string nameAxisX = "Пористость по ГИС, %";
    private string nameAxisY = "Пористость по керну, %";

    public CanvasViewModel()
    {
        //ClusteringService.Instance.WhenAnyValue(x => ClusteringService.Instance.kMeansClusterizer)
        //    .WhereNotNull()
        //    .Subscribe(x => UpdateData());
        UpdateGrid();
        ClusteringService.Instance.UpdateData += UpdateGrid;
        ClusteringService.Instance.UpdateData += UpdateData;
    }

    public double CanvasWidth
    {
        get => canvasWidth;
        set
        {
            this.RaiseAndSetIfChanged(ref canvasWidth, value);
        }
    }

    public double CanvasHeight
    {
        get => canvasHeight;
        set
        {
            this.RaiseAndSetIfChanged(ref canvasHeight, value);
        }
    }

    public ObservableCollection<GridLineViewModel> GridLines
    {
        get => gridLines;
        set
        {
            this.RaiseAndSetIfChanged(ref gridLines, value);
        }
    }

    public ObservableCollection<DataPointViewModel> ManualCentroids
    {
        get => manualCentroids;
        set
        {
            this.RaiseAndSetIfChanged(ref manualCentroids, value);
        }
    }

    public ObservableCollection<DataPointViewModel> DataPointList
    {
        get => dataPointList;
        set
        {
            this.RaiseAndSetIfChanged(ref dataPointList, value);
        }
    }

    public string NameAxisX
    {
        get => nameAxisX;
        set => this.RaiseAndSetIfChanged(ref nameAxisX, value);
    }

    public string NameAxisY
    {
        get => nameAxisY;
        set => this.RaiseAndSetIfChanged(ref nameAxisY, value);
    }

    private void UpdateGrid()
    {
        GridLines.Clear();

        double deltaX, minX, maxX;
        double deltaY, minY, maxY;

        if (DataPointList.Count > 0)
        {
            minX = DataPointList.Select(item => item.X).Min();
            maxX = DataPointList.Select(item => item.X).Max();
            minY = DataPointList.Select(item => item.Y).Min();
            maxY = DataPointList.Select(item => item.Y).Max();
            deltaX = maxX - minX;
            deltaY = maxY - minY;
        }
        else
        {
            minX = 0;
            maxX = 1;
            minY = 0;
            maxY = 1;
            deltaX = 1;
            deltaY = 1;
        }

        // Horizontal grid lines
        var stepSizeVer = (canvasHeight - deadZone * 2) / countGridLines;
        for (int k = deadZone; k < canvasHeight + stepSizeVer; k += (int)stepSizeVer)
        {
            var axisLabel = (minX + (deltaX * ((k - deadZone) / (canvasHeight - (deadZone * 2))))).ToString("F");
            var line = new GridLineViewModel(new Point(0, k), new Point(canvasWidth, k), axisLabel);
            GridLines.Add(line);
        }

        // Vertical grid lines
        var stepSizeHor = (canvasWidth - deadZone * 2) / countGridLines;
        for (int k = deadZone; k < canvasWidth + stepSizeHor; k += (int)stepSizeHor)
        {
            var axisLabel = (minY + (deltaY * ((k - deadZone) / (canvasHeight - (deadZone * 2))))).ToString("F");
            var line = new GridLineViewModel(new Point(k, 0), new Point(k, canvasHeight), axisLabel);
            GridLines.Add(line);
        }
    }

    public void UpdateData()
    {
        DataPointList.Clear();

        if (!ClusteringService.Instance.KMeansClusterizer!.IsIterated)
        {
            foreach (var data in ClusteringService.Instance.KMeansClusterizer!.Data)
            {
                DataPointList.Add(new DataPointViewModel(data.Values[0], data.Values[1]));
            }
        }
        else
        {
            for (int k = 0; k < ClusteringService.Instance.KMeansClusterizer!.Clusters.Length; k++)
            {
                

                foreach (var component in ClusteringService.Instance.KMeansClusterizer!.Clusters[k].Components)
                {
                    var color = (int)(new Random(k).NextSingle() * 16777215);

                    var dataUnit = new DataPointViewModel(
                        component.Values[0],
                        component.Values[1],
                        (new BrushConverter().ConvertFrom("#" + color.ToString("x4")) as IBrush)!);

                    DataPointList.Add(dataUnit);
                }
            }
        }

        var xMin = DataPointList.Min(unit => unit.X);
        var yMin = DataPointList.Min(unit => unit.Y);
        var xMax = DataPointList.Max(unit => unit.X);
        var yMax = DataPointList.Max(unit => unit.Y);
        var xScale = 600f / (xMax - xMin);
        var yScale = 600f / (yMax - yMin);

        for (int k = 0; k < DataPointList.Count; k++)
        {
            DataPointList[k] = new DataPointViewModel(
                (DataPointList[k].X - xMin * 1d) * xScale + 20d,
                (DataPointList[k].Y - yMin * 1d) * yScale + 20d,
                DataPointList[k].Brush!);
        }

        ManualCentroids.Clear();
        foreach (var cluster in ClusteringService.Instance.KMeansClusterizer!.Clusters)
        {
            ManualCentroids.Add(new DataPointViewModel(
                (cluster.Centroid.Values[0] - xMin * 1d) * xScale + 20d - 3d,
                (cluster.Centroid.Values[1] - yMin * 1d) * yScale + 20d - 3d,
                Brushes.Transparent));
        }
    }


    //private void PointerPressedHandler(object sender, PointerPressedEventArgs args)
    //{
    //    var point = args.GetCurrentPoint(sender as Control);
    //    var x = point.Position.X;
    //    var y = point.Position.Y;
    //    //var msg = $"Pointer press at {x}, {y} relative to sender.";
    //    //if (point.Properties.IsLeftButtonPressed)
    //    //{
    //    //    msg += " Left button pressed.";
    //    //}
    //    //if (point.Properties.IsRightButtonPressed)
    //    //{
    //    //    msg += " Right button pressed.";
    //    //}

    //    var fileSelector = DataContext as CanvasViewModel;
    //    //fileSelector!.ManualCentroids.Add(new DataPoint(x, y, Brushes.Black));
    //}
}
