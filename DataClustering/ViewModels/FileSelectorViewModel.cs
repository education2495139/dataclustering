﻿using ReactiveUI;
using System;
using System.Reactive.Linq;

namespace DataClustering.ViewModels;

public class FileSelectorViewModel : ViewModelBase
{
    private const string placeholder = $"Please select a .csv or .xls file!";

    private string pathString = placeholder;
    public string PathString
    {
        get => pathString;
        set => this.RaiseAndSetIfChanged(ref pathString, value);
    }

    private string rawData = string.Empty;
    public string RawData
    {
        get => rawData;
        set => this.RaiseAndSetIfChanged(ref rawData, value);
    }

    public FileSelectorViewModel()
    {
        this.WhenAnyValue(x => x.PathString)
            .Select(x => x = string.IsNullOrEmpty(x) ? placeholder : x)
            .Subscribe((text) => PathString = text);
    }
}
