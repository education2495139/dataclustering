﻿using DataClustering.Algorithms;
using DataClustering.Converters;
using DataClustering.Models;
using ReactiveUI;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;

namespace DataClustering.ViewModels;

public class MainViewModel : ViewModelBase
{
    public FileSelectorViewModel FileSelector { get; } = new FileSelectorViewModel();
    public CanvasViewModel Canvas { get; } = new CanvasViewModel();

    public ReactiveCommand<Unit, Unit> Iterate { get; }
    public ReactiveCommand<Unit, Unit> Compute { get; }

    public MainViewModel()
    {
        FileSelector.WhenAnyValue(x => x.RawData)
            .WhereNotNull()
            .Where(rawData => rawData != string.Empty)
            .Subscribe(CreateClusterizer);

        Iterate = ReactiveCommand.Create(() =>
        {
            ClusteringService.Instance.KMeansClusterizer?.Iterate();
            ClusteringService.Instance.UpdateData?.Invoke();
            Debug.WriteLine($"CanvasHeight {Canvas.CanvasHeight}");
        });

        Compute = ReactiveCommand.Create(() =>
        {
            ClusteringService.Instance.KMeansClusterizer?.Compute();
            ClusteringService.Instance.UpdateData?.Invoke();
        });
    }

    private void CreateClusterizer(string rawData)
    {
        var data = ConvertToDataVector.FromCSV(rawData);

        if (!string.IsNullOrEmpty(data.NameAxisX))
        {
            Canvas.NameAxisX = data.NameAxisX;
        }

        if (!string.IsNullOrEmpty(data.NameAxisY))
        {
            Canvas.NameAxisY = data.NameAxisY;
        }

        ClusteringService.Instance.CreateKMeansClusterizer(data.Vector, 4);
    }
}
