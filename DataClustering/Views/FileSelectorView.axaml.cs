using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Platform.Storage;
using System.Collections.Generic;
using System.IO;
using DataClustering.ViewModels;
using OfficeOpenXml;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace DataClustering.Views
{
    public partial class FileSelectorView : UserControl
    {
        public FileSelectorView()
        {
            InitializeComponent();
        }

        private async void OpenFileButton_Clicked(object sender, RoutedEventArgs args)
        {
            // Get top level from the current control. Alternatively, you can use Window reference instead.
            var topLevel = TopLevel.GetTopLevel(this);

            // Start async operation to open the dialog.
            var files = await topLevel!.StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions
            {
                FileTypeFilter = new[] { new FilePickerFileType("Select Excel file") { Patterns = new[] { "*.csv" } } },
                Title = "Open File",
                AllowMultiple = false
            });

            if (files.Count >= 1)
            {
                var fileType = Path.GetExtension(files[0].Path.AbsolutePath);
                string fileContent = string.Empty;

                switch (fileType)
                {
                    case ".csv":
                        await using(var stream = await files[0].OpenReadAsync())
                        {
                            using var streamReader = new StreamReader(stream);
                            fileContent = await streamReader.ReadToEndAsync();
                        }
                        break;

                    case ".xlsx":
                        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                        using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(files[0].Path.AbsolutePath)))
                        {
                            var myWorksheet = xlPackage.Workbook.Worksheets.First();
                            var totalRows = myWorksheet.Dimension.End.Row;
                            var totalColumns = myWorksheet.Dimension.End.Column;

                            for (int rowNum = 1; rowNum <= totalRows; rowNum++)
                            {
                                var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                                if (rowNum != 1)
                                {
                                    fileContent += "|";
                                }
                                fileContent += string.Join(";", row);
                            }

                            Debug.WriteLine(fileContent);
                        }
                        break;
                }

                var fileSelector = DataContext as FileSelectorViewModel;
                fileSelector!.RawData = fileContent;
                fileSelector!.PathString = files[0].Path.AbsolutePath;
                //(DataContext as FileSelectorViewModel)!.CreateClusterizerCommand.Execute((fileContent, files[0].Path.AbsolutePath));
            }
        }
    }
}
