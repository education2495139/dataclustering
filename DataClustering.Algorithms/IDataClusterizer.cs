﻿using System;

namespace DataClustering.Algorithms;

public interface IDataClusterizer<T>
{
    public T[] Data { get; }
    public ICluster<T>[] Clusters { get; }
    public bool IsIterated { get; }
    public void Iterate();
    public void Compute();
}
