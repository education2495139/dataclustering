﻿using System;

namespace DataClustering.Algorithms;

public interface IMeasurable<T>
{
    public T[] Values { get; }
    public T Measure(IMeasurable<T> targetCluster);
}
