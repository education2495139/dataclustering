﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace DataClustering.Algorithms.KMeans;

public class DataVector : IMeasurable<double>
{
    private double[] values;
    public double[] Values => values;

    public DataVector(double[] values)
    {
        this.values = values;
    }

    public DataVector(DataVector vector)
    {
        values = vector.values;
    }

    public double Measure(IMeasurable<double> centroid)
    {
        var vector = this - (DataVector)centroid;
        var sum = 0d;
        foreach (var value in vector.Values)
        {
            sum += (value * value);
        }
        return sum;
    }

    public static DataVector GetZero(int dimension)
    {
        return new DataVector(new double[dimension].Select(item => item = 0d).ToArray());
    }

    public static DataVector operator +(DataVector a, DataVector b)
    {
        var numberArray = new double[a.values.Length];
        for (int k = 0; k < a.values.Length; k++)
            numberArray[k] = a.values[k] + b.values[k];
        return new DataVector(numberArray);
    }

    public static DataVector operator -(DataVector a, DataVector b)
    {
        var numberArray = new double[a.values.Length];
        for (int k = 0; k < a.values.Length; k++)
            numberArray[k] = a.values[k] - b.values[k];
        return new DataVector(numberArray);
    }

    public static DataVector operator *(DataVector a, DataVector b)
    {
        var numberArray = new double[a.values.Length];
        for (int k = 0; k < a.values.Length; k++)
            numberArray[k] = a.values[k] * b.values[k];
        return new DataVector(numberArray);
    }

    public static DataVector operator /(DataVector a, int b)
    {
        var numberArray = new double[a.values.Length];
        for (int k = 0; k < a.values.Length; k++)
            numberArray[k] = a.values[k] / (double)b;
        return new DataVector(numberArray);
    }
}
