﻿using System.Diagnostics;
using System.Numerics;

namespace DataClustering.Algorithms.KMeans;

public class KMeansClusterizer : IDataClusterizer<DataVector>
{
    private const int maxIterations = 50;
    private const int minMovability = 1;
    private int currentIteration = 0;

    private bool isIterated = false;
    public bool IsIterated => isIterated;

    private DataVector[] data;
    public DataVector[] Data => data;

    private ICluster<DataVector>[] clusters;
    public ICluster<DataVector>[] Clusters => clusters;

    public KMeansClusterizer(DataVector[] data, int defaultClustersCount)
    {
        this.data = data;
        clusters = GetRandomCentroids(data, defaultClustersCount);
    }

    public KMeansClusterizer(DataVector[] data, ICluster<DataVector>[] defaultClusters) : this(data, defaultClusters.Length)
    {
        clusters = defaultClusters;
    }

    private ICluster<DataVector>[] GetRandomCentroids(IList<DataVector> data, int count)
    {
        if (data.Count < count) throw new ArgumentException("The number of data elements is less than the number of clusters.");
        var random = new Random();
        return data
            .OrderBy(_ => random.Next())
            .Take(count)
            .Select(item => new Cluster(item))
            .ToArray();
    }

    public void Iterate()
    {
        isIterated = true;

        currentIteration++;

        foreach (var cluster in clusters)
        {
            cluster.Components.Clear();
        }

        foreach (var vector in data)
        {
            GetClosestCluster(vector).Components.Add(vector);
        }

        var distanceChangeTotal = 0d;
        foreach (var cluster in clusters)
        {
            distanceChangeTotal += cluster.UpdateCentroid();
        }

        if (distanceChangeTotal < minMovability)
        {
            currentIteration = maxIterations;
        }
    }

    public void Compute()
    {
        for (int k = 0; k < maxIterations; k++)
        {
            Iterate();
        }
    }

    private ICluster<DataVector> GetClosestCluster(DataVector unitData)
    {
        return clusters
            .Select(item => new { measure = unitData.Measure(item.Centroid), cluster = item })
            .MinBy(item => item.measure)
            !.cluster;
    }
}
