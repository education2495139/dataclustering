﻿using System;

namespace DataClustering.Algorithms.KMeans;

public class Cluster : ICluster<DataVector>
{
    private DataVector lastCentroid;
    private DataVector centroid;
    public DataVector Centroid => centroid;

    private IList<DataVector> _components = new List<DataVector>();
    public IList<DataVector> Components => _components;

    public Cluster(DataVector defaultCentroid)
    {
        centroid = defaultCentroid;
        lastCentroid = defaultCentroid;
    }

    public double GetDistanceSq(DataVector a, DataVector b)
    {
        return Convert.ToDouble((a - b).Values.Aggregate((sum, num) => ((num * num) + sum)));
    }

    public double UpdateCentroid()
    {
        lastCentroid = centroid;
        var zeroVector = DataVector.GetZero(centroid.Values.Length);
        centroid = Components.Aggregate(zeroVector, (sum, item) => sum + item, sum => sum / Components.Count);

        return GetDistanceSq(centroid, lastCentroid);
    }
}
