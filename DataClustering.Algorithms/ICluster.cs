﻿using System;

namespace DataClustering.Algorithms;

public interface ICluster<T>
{
    public T Centroid { get; }
    public IList<T> Components { get; }
    public double UpdateCentroid();
}
